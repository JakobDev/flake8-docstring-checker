# flake8-docstring-checker

# [Moved to Codeberg](https://codeberg.org/JakobDev/flake8-docstring-checker)

A simple flake8 plugin that checks if everything has a docstring. This plugin does not more than that. If you want a plugin which more functions you should take a look at [flake8-docstrings](https://pypi.org/project/flake8-docstrings/).

### List of warnings:

| ID | Description |
| ------ | ------ |
| DC100 | Module has no docstring |
| DC101 | Missing docstring in class |
| DC102 | Missing docstring in public function |
| DC103 | Missing docstring in private function |
| DC104 | Missing docstring in special function |
