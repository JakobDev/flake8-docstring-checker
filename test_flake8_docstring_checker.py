from flake8_docstring_checker import Plugin
import ast


testcode = """
def func_a():
    "Test"
    pass


def func_b():
    pass


class class_a:
    "Test"
    pass


class class_b:
    pass


class class_c:
    "Test"
    def __init__(self):
        pass

    def _private(self):
        pass
"""


def test_flake8_docstring_checker():
    tree = ast.parse(testcode)
    plugin = Plugin(tree)
    results = list(plugin.run())
    assert (0, 0, "DC100 Module has no docstring", Plugin) in results
    assert (7, 0, "DC102 Missing docstring in public function func_b", Plugin) in results
    assert (16, 0, "DC101 Missing docstring in class class_b", Plugin) in results
    assert (22, 4, "DC104 Missing docstring in special function __init__", Plugin) in results
    assert (25, 4, "DC103 Missing docstring in private function _private", Plugin) in results
